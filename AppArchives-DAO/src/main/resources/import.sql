INSERT INTO `user` (firstname, hashedpassword, lastname, login, role, salt) VALUES ('Véronique', 'c57717aa2c45527d368bd245aae3f38c5e961fb06b340a48a6f99aa393f73db8', 'Tienne', 'employee', 'Employee', '[B@579bebc3');
INSERT INTO `user` (firstname, hashedpassword, lastname, login, role, salt) VALUES ('Lucie', '355ff7e2c305d510cb45bb7e1ef38cd4e1958fb0513efdd0af40d1ed8b54d6af', 'Martin', 'employee1', 'Employee', '[B@6b86b808');
INSERT INTO `user` (firstname, hashedpassword, lastname, login, role, salt) VALUES ('Marc', 'a465107bb7d62483d712756b5843243287cbe62384097d318372ebfbcc37e7a4', 'Durand', 'reader1', 'Reader', '[B@4118c9d0');


INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '3E37', 'Mariage', 1, '1910-01-01', '1910-12-31', 'Registre des mariages de 1910');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '3E38', 'Mariage', 1, '1911-01-01', '1911-12-31', 'Registre des mariages de 1911');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E39', 'Mariage', 0, '1912-01-01', '1912-12-31', 'Registre des mariages de 1912');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '3E40', 'Mariage', 1, '1913-01-01', '1913-12-31', 'Registre des mariages de 1913');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '3E41', 'Mariage', 1, '1914-01-01', '1914-12-31', 'Registre des mariages de 1914');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E42', 'Mariage', 0, '1915-01-01', '1915-12-31', 'Registre des mariages de 1915');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E43', 'Mariage', 0, '1916-01-01', '1916-12-31', 'Registre des mariages de 1916');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E44', 'Mariage', 0, '1917-01-01', '1917-12-31', 'Registre des mariages de 1917');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E45', 'Mariage', 0, '1918-01-01', '1918-12-31', 'Registre des mariages de 1918');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E46', 'Mariage', 0, '1919-01-01', '1919-12-31', 'Registre des mariages de 1919');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '3E47', 'Mariage', 0, '1920-01-01', '1920-12-31', 'Registre des mariages de 1920');

INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '2E77', 'Naissance', 1, '1911-01-01', '1911-07-20', 'Registre des naissances du 1er janvier au 20 juillet 1911');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '2E78', 'Naissance', 1, '1911-07-20', '1911-12-30', 'Registre des naissances du 20 juillet 1911 au 30 décembre 1911');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E79', 'Naissance', 0, '1912-01-01', '1912-12-31', 'Registre des naissances du 1er janvier au 31 décembre 1912');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E80', 'Naissance', 0, '1913-01-01', '1913-12-31', 'Registre des naissances du 1er janvier au 31 décembre 1913');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '2E81', 'Naissance', 1, '1914-01-01', '1914-06-30', 'Registre des naissances du 1er janvier au 30 juin 1914');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E82', 'Naissance', 1, '1914-06-30', '1914-12-28', 'Registre des naissances du 30 juin 1914 au 28 décembre 1914');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E83', 'Naissance', 0, '1915-01-01', '1915-12-31', 'Registre des naissances du 1er janvier au 31 décembre 1915');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E83', 'Naissance', 0, '1915-01-01', '1915-12-31', 'Registre des naissances du 1er janvier au 31 décembre 1915');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '2E84', 'Naissance', 0, '1916-01-01', '1916-12-31', 'Registre des naissances du 1er janvier au 31 décembre 1916');

INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E73', 'Décès', 0, '1910-01-01', '1910-07-03', 'Registre des décès du 1er janvier au 3 juillet 1910');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E74', 'Décès', 0, '1910-07-03', '1910-12-30', 'Registre des décès du 3 juillet au 30 décembre 1910');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E75', 'Décès', 0, '1911-01-01', '1911-07-18', 'Registre des décès du 1er janvier au 18 juillet 1911');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E76', 'Décès', 0, '1911-07-18', '1911-12-30', 'Registre des décès du 18 juillet au 30 décembre 1911');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E77', 'Décès', 0, '1912-01-01', '1912-06-14', 'Registre des décès du 1er janvier au 14 juin 1912');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (1, '4E78', 'Décès', 0, '1912-06-14', '1912-12-30', 'Registre des décès du 14 juin au 30 décembre 1912');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '4E79', 'Décès', 1, '1913-01-01', '1913-07-03', 'Registre des décès du 1er janvier au 3 juillet 1913');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '4E80', 'Décès', 1, '1913-07-03', '1913-12-31', 'Registre des décès du 3 juillet au 31 décembre 1913');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '4E81', 'Décès', 1, '1914-01-01', '1914-06-29', 'Registre des décès du 1er janvier au 29 juin 1914');
INSERT INTO `archive` (availability, call_numbers, category, digitalized, latest_date, earliest_date, title) VALUES (0, '4E82', 'Décès', 1, '1914-06-29', '1914-12-31', 'Registre des décès du 29 juin au 31 décembre 1914');


INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('7.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('8.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('9.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('10.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('11.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);
INSERT INTO `file` (name, path, archive_id) VALUES ('12.PNG', './resources/archives/registres/mariages/3E37_1910/', 1);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);
INSERT INTO `file` (name, path, archive_id) VALUES ('7.PNG', './resources/archives/registres/mariages/3E38_1911/', 2);


INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('7.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('8.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('9.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);
INSERT INTO `file` (name, path, archive_id) VALUES ('10.PNG', './resources/archives/registres/mariages/3E40_1913/', 4);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('7.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('8.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('9.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);
INSERT INTO `file` (name, path, archive_id) VALUES ('10.PNG', './resources/archives/registres/mariages/3E41_1914/', 5);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);
INSERT INTO `file` (name, path, archive_id) VALUES ('7.PNG', './resources/archives/registres/naissances/2E77_jan-juil1911/', 12);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/naissances/2E78_juil-dec1911/', 13);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/naissances/2E81_jan-juin1914/', 16);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);
INSERT INTO `file` (name, path, archive_id) VALUES ('5.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);
INSERT INTO `file` (name, path, archive_id) VALUES ('6.PNG', './resources/archives/registres/naissances/2E82_juin-dec1914/', 17);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/deces/4E79_1913/', 27);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/deces/4E79_1913/', 27);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/deces/4E79_1913/', 27);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/deces/4E79_1913/', 27);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/deces/4E80_1913/', 28);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/deces/4E80_1913/', 28);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/deces/4E80_1913/', 28);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/deces/4E80_1913/', 28);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/deces/4E81_1914/', 29);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/deces/4E81_1914/', 29);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/deces/4E81_1914/', 29);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/deces/4E81_1914/', 29);

INSERT INTO `file` (name, path, archive_id) VALUES ('1.PNG', './resources/archives/registres/deces/4E82_1914/', 30);
INSERT INTO `file` (name, path, archive_id) VALUES ('2.PNG', './resources/archives/registres/deces/4E82_1914/', 30);
INSERT INTO `file` (name, path, archive_id) VALUES ('3.PNG', './resources/archives/registres/deces/4E82_1914/', 30);
INSERT INTO `file` (name, path, archive_id) VALUES ('4.PNG', './resources/archives/registres/deces/4E82_1914/', 30);



