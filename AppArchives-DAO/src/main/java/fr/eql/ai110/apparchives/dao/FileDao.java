package fr.eql.ai110.apparchives.dao;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.File;
import fr.eql.ai110.apparchives.idao.FileIDao;

@Remote(FileIDao.class)
@Stateless
public class FileDao extends GenericDao<File> implements FileIDao{

	@Override
	public Set<File> findFilesByArchive(Archive archive) {
		Query query = em.createQuery("SELECT f FROM File f WHERE f.archive = :archiveParam");
		query.setParameter("archiveParam", archive);
		return new LinkedHashSet<File>(query.getResultList());
	}

}
