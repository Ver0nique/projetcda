package fr.eql.ai110.apparchives.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.idao.ArchiveIDao;

@Remote(ArchiveIDao.class)
@Stateless
public class ArchiveDao extends GenericDao<Archive> implements ArchiveIDao {

	@Override
	public Set<Archive> findArchivesByReservation(Reservation reservation) {
		Query query = em.createQuery("SELECT a FROM Archive a INNER JOIN a.reservations r WHERE r = :reservationParam");
		query.setParameter("reservationParam", reservation);
		return new LinkedHashSet<Archive>(query.getResultList());
	}

	@Override
	public Archive getArchive(int id) {
		return getById(id);
	}

	@Override
	public void addArchiveToUserBasket(int userId, int archiveId) {
		Archive archive = getArchive(archiveId);
		archive.getUser().setId(userId);
	}

	@Override
	public Set<Archive> findArchivesByMulticriteriaSearch(String callNumbers, String category, Boolean digitalized, String title) {
		callNumbers = "%" + callNumbers + "%";
		title = "%" + title + "%";
		Query query = em.createQuery("SELECT a FROM Archive a WHERE (a.category = :categoryParam OR :categoryParam is null)"
				+ "AND (a.digitalized = :digitalizedParam OR :digitalizedParam is null)"
				+ "AND (a.callNumbers LIKE :callNumbersParam)"
				+ "AND (a.title LIKE :titleParam)"
				);
		
		query.setParameter("callNumbersParam", callNumbers);
		query.setParameter("categoryParam", category);
		query.setParameter("digitalizedParam", digitalized);
		query.setParameter("titleParam", title);
		return new LinkedHashSet<Archive>(query.getResultList());
	}

	@Override
	public List<String> findArchivesCallnumbers() {
		Query query = em.createQuery("SELECT a.callNumbers FROM Archive a");
		return new ArrayList<String>(query.getResultList());
	}

	@Override
	public List<String> findArchivesTitle() {
		Query query = em.createQuery("SELECT a.title FROM Archive a");
		return new ArrayList<String>(query.getResultList());
	}

	

}
