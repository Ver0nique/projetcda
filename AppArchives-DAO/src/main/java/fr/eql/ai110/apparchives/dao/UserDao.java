package fr.eql.ai110.apparchives.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.idao.UserIDao;

@Remote(UserIDao.class)
@Stateless
public class UserDao extends GenericDao<User> implements UserIDao{

	@Override
	public User authenticate(String login, String hashedPassword) {
		User user = null;
		List<User> users;
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :loginParam AND u.hashedpassword = :hashedPasswordParam");
		query.setParameter("loginParam", login);
		query.setParameter("hashedPasswordParam", hashedPassword);
		users = query.getResultList();
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;

	}

	@Override
	public String getSalt(String login) {
		String salt = null;
		List<String> salts;
		Query query = em.createQuery("SELECT u.salt FROM User u WHERE u.login = :loginParam");
		query.setParameter("loginParam", login);
		salts = query.getResultList();
		if (salts.size() > 0) {
			salt = salts.get(0);
		}
		return salt;

	}

	@Override
	public Boolean existLogin(User user) {
		Boolean result = true;
		List<Boolean> results;
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :loginParam");
		query.setParameter("loginParam", user.getLogin());
		results = query.getResultList();
		if (results.size() == 0) {
			result = false;
		}
		return result;
	}

}
