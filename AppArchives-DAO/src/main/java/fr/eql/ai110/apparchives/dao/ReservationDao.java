package fr.eql.ai110.apparchives.dao;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.idao.ReservationIDao;

@Remote(ReservationIDao.class)
@Stateless
public class ReservationDao extends GenericDao<Reservation> implements ReservationIDao {

	@Override
	public Set<Reservation> findReservationsByUser(User user) {
		Query query = em.createQuery("SELECT r FROM Reservation r WHERE r.user = :userParam");
		query.setParameter("userParam", user);
		return new LinkedHashSet<Reservation>(query.getResultList());

	}

}
