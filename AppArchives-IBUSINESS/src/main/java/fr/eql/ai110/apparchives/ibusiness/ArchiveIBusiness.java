package fr.eql.ai110.apparchives.ibusiness;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;

public interface ArchiveIBusiness {

	public List<Archive> getArchives();
	public Archive getArchive(int id);
	public Set<Archive> getArchivesByReservation(Reservation reservation);
	public void addToUserBasket(int userId, int archiveId);
	public Archive save(Archive archive);
	public Set<Archive> search(String callNumbers, String category, Boolean digitalized, String title, String earliestYear, String latestYear);
	public List<String> getArchivesCallnumbers();
	public List<String> getArchivesTitle();
	public List<String> getArchivesCategory();
	public List<String> getArchivesYear();
}
