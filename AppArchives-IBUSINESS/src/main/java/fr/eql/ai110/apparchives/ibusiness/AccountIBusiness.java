package fr.eql.ai110.apparchives.ibusiness;

import java.util.List;

import fr.eql.ai110.apparchives.entity.User;

public interface AccountIBusiness {
	
	public boolean create(User user, String password);
	public User connect(String login, String password);
	public List<User> getUsers();
	public User getUser(int id);

}
