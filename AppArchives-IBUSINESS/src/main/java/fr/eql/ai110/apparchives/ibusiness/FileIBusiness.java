package fr.eql.ai110.apparchives.ibusiness;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.File;

public interface FileIBusiness {

	public Set<File> getFilesByArchive(Archive archive);
	public List<File> getFiles();
	public File getFile(int id);
	
}
