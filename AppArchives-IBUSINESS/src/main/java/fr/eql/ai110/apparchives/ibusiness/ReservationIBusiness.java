package fr.eql.ai110.apparchives.ibusiness;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;

public interface ReservationIBusiness {
	
	public List<Reservation> getReservations();
	public Reservation getReservation(int id);
	public void createReservation(User user, Set<Archive> archives, LocalDate appointmentDate);
	public void validateReservation(Reservation reservation);
	public void refuseReservation(Reservation reservation);
	public void cancelReservation(Reservation reservation);
	public void closeReservation(Reservation reservation);
	public Set<Reservation> getReservationsByUser(User user);
	public void automaticCancellation(Reservation reservation);

}
