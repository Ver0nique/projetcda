package fr.eql.ai110.apparchives.business;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.ibusiness.ReservationIBusiness;
import fr.eql.ai110.apparchives.idao.ArchiveIDao;
import fr.eql.ai110.apparchives.idao.ReservationIDao;
import fr.eql.ai110.apparchives.idao.UserIDao;

/**
 * 
 * @author Véronique Tienne
 *
 */
@Remote(ReservationIBusiness.class)
@Stateless
public class ReservationBusiness implements ReservationIBusiness {

	@EJB
	private ReservationIDao reservationDao;
	@EJB
	private ArchiveIDao archiveDao;
	@EJB
	private UserIDao userDao;

	/**
	 * Retourne la liste des réservations
	 * @return la liste des réservations
	 */
	@Override
	public List<Reservation> getReservations() {
		List<Reservation> list = reservationDao.getAll();
		for (Reservation reservation : list) {
			automaticCancellation(reservation);
		}
		return list;
	}

	/**
	 * Retourne une réservation à partir de son id
	 * @id : id de la réservation recherchée
	 * @return une réservation à partir de son id
	 */
	@Override
	public Reservation getReservation(int id) {
		Reservation r = reservationDao.getById(id);
		automaticCancellation(r);
		return r;
	}

	/**
	 * Crée une réservation
	 * @param user : utilisateur effectuant la réservation
	 * @param archives : set d'archives à réserver
	 * @appointmentDate : date de rendez-vous choisie par l'utilisateur
	 */
	@Override
	public void createReservation(User user, Set<Archive> archives, LocalDate appointmentDate) {
		for (Archive archive : archives) {
			archive.setAvailability(false);
			archiveDao.update(archive);
			}
		Reservation reservation = new Reservation();
		reservation.setAppointmentDate(appointmentDate);
		reservation.getReservedArchives().addAll(archives);
		reservation.setCreationDate(LocalDate.now());
		reservation.setUser(user);
		reservationDao.add(reservation);
	}

	/**
	 * Valide la réservation à la date du jour
	 * @param reservation à valider
	 */
	@Override
	public void validateReservation(Reservation reservation) {
		reservation.setValidationDate(LocalDate.now());
		reservationDao.update(reservation);
	}

	/**
	 * Refuse la réservation à la date du jour
	 * @param reservation à refuser
	 */
	@Override
	public void refuseReservation(Reservation reservation) {
		Set<Archive> set = archiveDao.findArchivesByReservation(reservation);
		for (Archive archive : set) {
			archive.setAvailability(true);
			archiveDao.update(archive);
		}
		reservation.setRefusalDate(LocalDate.now());
		reservationDao.update(reservation);	
	}

	/**
	 * Annule la réservation à la date du jour
	 * @param reservation à annuler
	 */
	@Override
	public void cancelReservation(Reservation reservation) {
		Set<Archive> set = archiveDao.findArchivesByReservation(reservation);
		for (Archive archive : set) {
			archive.setAvailability(true);
			archiveDao.update(archive);
		}
		reservation.setCancellationDate(LocalDate.now());
		reservationDao.update(reservation);
	}

	/**
	 * Clôture la réservation à la date du jour
	 * @param reservation à clôturer
	 */
	@Override
	public void closeReservation(Reservation reservation) {
		Set<Archive> set = archiveDao.findArchivesByReservation(reservation);
		for (Archive archive : set) {
			archive.setAvailability(true);
			archiveDao.update(archive);
		}
		reservation.setCompletionDate(LocalDate.now());
		reservationDao.update(reservation);
	}

	/**
	 * Retourne le set des réservations d'un utilisateur
	 * @param user : utilisateur dont on souhaite récupérer la liste de réservations
	 * @return le set des réservations d'un utilisateur
	 */
	@Override
	public Set<Reservation> getReservationsByUser(User user) {
		Set<Reservation> set = reservationDao.findReservationsByUser(user);
		for (Reservation reservation : set) {
			automaticCancellation(reservation);
		}
		return set;
	}

	/**
	 * Annule automatiquement une réservation lorsque la date de rendez-vous est antérieur à la date du jour
	 * @param reservation à annuler automatiquement
	 */
	@Override
	public void automaticCancellation(Reservation reservation) {
		if(reservation.getAppointmentDate().isBefore(LocalDate.now()) 
				&& reservation.getCompletionDate() == null 
				&& reservation.getRefusalDate() == null ) {
			cancelReservation(reservation);
		}
	}




}
