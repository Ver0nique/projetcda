package fr.eql.ai110.apparchives.business;

import java.security.MessageDigest;

import java.security.SecureRandom;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.ibusiness.AccountIBusiness;
import fr.eql.ai110.apparchives.idao.UserIDao;

/**
 * 
 * @author Véronique Tienne
 *
 */
@Remote(AccountIBusiness.class)
@Stateless
public class AccountBusiness implements AccountIBusiness {


	@EJB
	private UserIDao userDao;

	/**
	 * Crée un nouvel utilisateur
	 * 
	 * @param user : entité initialisée à partir des données saisies dans le formulaire d'inscription
	 * @param password : mot de passe renseigné dans le formulaire d'inscription
	 * @return false si l'identifiant (login) existe déjà dans la base de données, true sinon
	 * 
	 */
	@Override
	public boolean create(User user, String password) {
		boolean isCreated = false;
		if(!userDao.existLogin(user)) {
			String salt = generateSalt();
			user.setSalt(salt);
			String hashedPassword = generateHashedPassword(salt, password);	
			user.setHashedpassword(hashedPassword);
			userDao.add(user);
			isCreated = true;
		}
		return isCreated;
	}

	/**
	 * Authentifie un utilisateur
	 * 
	 * @param login : identifiant saisi dans le formulaire de connexion
	 * @param password : mot de passe saisi dans le formulaire de connexion
	 * @return l'entité utilisateur correspondant, null si les identifiants sont incorrects
	 */
	@Override
	public User connect(String login, String password) {
		User result = null;
		String salt = userDao.getSalt(login);
		if(salt != null) {
			String hashedPassword = generateHashedPassword(salt, password);		
			result = userDao.authenticate(login, hashedPassword);
		}
		return result;

	}

	/**
	 * @return la laste complète des utilisateurs
	 */
	@Override
	public List<User> getUsers() {
		return userDao.getAll();
	}

	/**
	 * @param id : id d'un utilisateur
	 * @return l'utilisateur à qui correspond l'id
	 */
	@Override
	public User getUser(int id) {
		return userDao.getById(id);
	}
	
	/**
	 * Génère un hash à partir du mot de passe de l'utilisateur et d'un salt
	 * 
	 * @param salt : stocké en base de données, il a été attribué aléatoirement à l'utilisateur au moment de l'inscription
	 * @param password : mot de passe de l'utilisateur
	 * @return un hash
	 */
	private String generateHashedPassword(String salt, String password) {
		String saltedPassword = password + salt; 
		String hashedPassword = null;
		 try{
		        MessageDigest digest = MessageDigest.getInstance("SHA-256");
		        byte[] hash = digest.digest(saltedPassword.getBytes("UTF-8"));
		        StringBuffer hexString = new StringBuffer();

		        for (int i = 0; i < hash.length; i++) {
		            String hex = Integer.toHexString(0xff & hash[i]);
		            if(hex.length() == 1) hexString.append('0');
		            hexString.append(hex);
		        }
		        hashedPassword = hexString.toString();
		    } catch(Exception ex){
		       throw new RuntimeException(ex);
		    }
	
		return hashedPassword;
	}
	
	/**
	 * Renvoie un salt calculé de manière aléatoire
	 * @return un salt 
	 */
	private String generateSalt() {
		SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt.toString();
	}




}
