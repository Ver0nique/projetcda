package fr.eql.ai110.apparchives.business;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.File;
import fr.eql.ai110.apparchives.ibusiness.FileIBusiness;
import fr.eql.ai110.apparchives.idao.FileIDao;

/**
 * 
 * @author Véronique Tienne
 *
 */
@Remote(FileIBusiness.class)
@Stateless
public class FileBusiness implements FileIBusiness{
	
	@EJB
	FileIDao fileDao;
	
	/**
	 * Retourne le set de File appartenant à une archive
	 * @param archive : archive dont on souhaite la liste de File
	 * @return le set de File appartenant à une archive
	 */
	@Override
	public Set<File> getFilesByArchive(Archive archive) {
		return fileDao.findFilesByArchive(archive);
	}

	/**
	 * Retourne la liste de tous les Files
	 * @return la liste de tous les Files
	 */
	@Override
	public List<File> getFiles() {
		return fileDao.getAll();
	}

	/**
	 * Retourne un File à partir de son id
	 * @param id : id du File recherché
	 * @return File à partir de son id
	 */
	@Override
	public File getFile(int id) {
		return fileDao.getById(id);
	}

}
