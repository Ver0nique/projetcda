package fr.eql.ai110.apparchives.business;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.ibusiness.ArchiveIBusiness;
import fr.eql.ai110.apparchives.idao.ArchiveIDao;

/**
 * 
 * @author Véronique Tienne
 *
 */
@Remote(ArchiveIBusiness.class)
@Stateless
public class ArchiveBusiness implements ArchiveIBusiness {

	@EJB
	private ArchiveIDao archiveDao;

	/**
	 * Retourne la liste complète des archives
	 * @return la liste complète des archives
	 */
	@Override
	public List<Archive> getArchives() {
		return archiveDao.getAll();
	}

	/**
	 * Retourne l'archive correspondant à l'id
	 * @param id : id de l'archive recherchée
	 * @return archive correspondant à l'id
	 */
	@Override
	public Archive getArchive(int id) {
		return archiveDao.getById(id);
	}

	/**
	 * Permet d'ajouter des archives dans le panier de l'utilisateur connecté
	 * @param userId : id de l'utilisateur connecté
	 * @param archiveId : id de l'archive à ajouter au panier
	 */
	@Override
	public void addToUserBasket(int userId, int archiveId) {
		archiveDao.addArchiveToUserBasket(userId, archiveId);
	}

	/**
	 * Permet de mettre à jour une archive
	 * @param archive : archive à mettre à jour
	 */
	@Override
	public Archive save(Archive archive) {
		return archiveDao.update(archive);
	}

	/**
	 * Retourne la liste des archives appartenant à une réservation
	 * @param reservation : reservation dont on souhaite obtenir la liste des archives
	 * @return la liste des archives de la réservation
	 */
	@Override
	public Set<Archive> getArchivesByReservation(Reservation reservation) {
		return archiveDao.findArchivesByReservation(reservation);
	}
	
	/**
	 * Retourne la liste des cotes des archives
	 * @return la liste des cotes des archives
	 */
	@Override
	public List<String> getArchivesCallnumbers() {
		return archiveDao.findArchivesCallnumbers();
	}

	/**
	 * Retourne un set d'archives correspondant à la recherche multicritères
	 * @param callNumbers : cote de l'archive recherchée
	 * @param category : catégorie de l'archive recherchée
	 * @param digitalized : choix du format (numérisé ou non) de l'archive recherché
	 * @param title : titre de l'archive recherchée
	 * @param earliestYear : année la plus ancienne de l'archive recherchée
	 * @param latestYear : année la plus récente de l'archive recherchée
	 * @return le set d'archives correspondant à la recherche multicritères
	 */
	@Override
	public Set<Archive> search(String callNumbers, String category, Boolean digitalized, String title, String earliestYear, String latestYear) {
		Set<Archive> set = archiveDao.findArchivesByMulticriteriaSearch(callNumbers, category, digitalized, title);
		Set<Archive> results = new LinkedHashSet<>();
		if(earliestYear.equals("") && latestYear.equals("")) {
			results.addAll(set);
		}else {
			for (Archive archive : set) {
				if(earliestYear.equals("")) {
					LocalDate latestDate = LocalDate.of(Integer.valueOf(latestYear),12,31);
					if(archive.getLatestDate().isBefore(latestDate) || archive.getLatestDate().isEqual(latestDate)) {
						results.add(archive);
					}
				}else if(latestYear.equals("")) {
					LocalDate earliestDate = LocalDate.of(Integer.valueOf(earliestYear),1,1);
					if(archive.getEarliestDate().isAfter(earliestDate) || archive.getEarliestDate().isEqual(earliestDate)) {
						results.add(archive);
					}
				}else {
					LocalDate earliestDate = LocalDate.of(Integer.valueOf(earliestYear),1,1);
					LocalDate latestDate = LocalDate.of(Integer.valueOf(latestYear),12,31);
					if(archive.getEarliestDate().isAfter(earliestDate) || archive.getEarliestDate().isEqual(earliestDate)) {
						if(archive.getLatestDate().isBefore(latestDate) || archive.getLatestDate().isEqual(latestDate)) {
							results.add(archive);
						}
					}
				}
			}
		}
		return results;
	}

	/**
	 * Retourne la liste de tous les titres d'archives
	 * @return la liste de tous les titres d'archives
	 */
	@Override
	public List<String> getArchivesTitle() {
		return archiveDao.findArchivesTitle();
	}

	/**
	 * Retourne la liste des catégories d'archives
	 * @return la liste des catégories d'archives
	 */
	@Override
	public List<String> getArchivesCategory() {
		List<String> categories = new ArrayList<String>(Arrays.asList("Décès","Mariage","Naissance"));
		return categories;
	}

	/**
	 * Retourne la liste des années des archives
	 * @return la liste des années des archives
	 */
	@Override
	public List<String> getArchivesYear() {
		List<String> years = new ArrayList<String>();
		for(int i=1910; i<1921; i++) {
			years.add(String.valueOf(i));
		}
		return years;
	}




}
