package fr.eql.ai110.apparchives.idao;

import java.util.Set;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.File;

public interface FileIDao extends GenericIDao<File> {
	
	Set<File> findFilesByArchive(Archive archive);

}
