package fr.eql.ai110.apparchives.idao;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;

public interface ArchiveIDao extends GenericIDao<Archive> {
	
	Set<Archive> findArchivesByReservation(Reservation reservation);
	Archive getArchive(int id);
	void addArchiveToUserBasket(int userId, int archiveId);
	Set<Archive> findArchivesByMulticriteriaSearch(String callNumbers, String category, Boolean digitalized, String title);
	List<String> findArchivesCallnumbers();
	List<String> findArchivesTitle();

}
