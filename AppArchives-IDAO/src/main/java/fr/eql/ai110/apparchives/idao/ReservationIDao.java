package fr.eql.ai110.apparchives.idao;

import java.util.Set;

import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;

public interface ReservationIDao extends GenericIDao<Reservation>{

	Set<Reservation> findReservationsByUser(User user);
}
