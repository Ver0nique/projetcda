package fr.eql.ai110.apparchives.idao;

import fr.eql.ai110.apparchives.entity.User;

public interface UserIDao extends GenericIDao<User>{
	
	User authenticate(String login, String hashedPassword);
	String getSalt(String login);
	Boolean existLogin(User user);

}
