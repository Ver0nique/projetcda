package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.ibusiness.ReservationIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbReservation")
@RequestScoped
public class ReservationManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbUser.user}")
	private User connectedUser;
	
	@ManagedProperty(value="#{mbBasket.basket}")
	private Set<Archive> connectedUserBasket;
	
	private Date userAppointmentDate;
	
	private List<Reservation> reservations;
	private Reservation reservation;
	private Set<Reservation> connectedUserReservations;
	
	@EJB
	private ReservationIBusiness reservationBusiness;

	@PostConstruct
	public void init(){
		connectedUserReservations =  reservationBusiness.getReservationsByUser(connectedUser);
	}
	
	public String sendReservationRequest() {
		String forward = "";
		LocalDate appDate = convertToLocalDateViaSqlDate(userAppointmentDate);
		if(appDate.isBefore(LocalDate.now())) {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Veuillez saisir une date postérieure à la date du jour", "");
			FacesContext.getCurrentInstance().addMessage("reservationForm:cdate", facesMessage);
		}else if (appDate.getDayOfWeek().getValue() == 6 || appDate.getDayOfWeek().getValue() == 7){
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Veuillez saisir un jour ouvré", "");
			FacesContext.getCurrentInstance().addMessage("reservationForm:cdate", facesMessage);
		}else if(connectedUserBasket.isEmpty()) {
			FacesMessages.warning("", "Votre panier est vide, veuillez ajouter des archives dans votre panier");
		}else {
			reservationBusiness.createReservation(connectedUser, connectedUserBasket, appDate);
			connectedUserBasket.clear();
		    FacesMessages.info("", "Votre réservation a bien été enregistrée.");
			forward = "/search.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public void validate(Reservation r) {
		reservationBusiness.validateReservation(r);
		FacesMessages.info("", "La validation de la réservation a bien été enregistrée.");

	}
	
	public void refuse(Reservation r) {
		reservationBusiness.refuseReservation(r);
		FacesMessages.info("", "Le refus de la réservation a bien été enregistré.");
	}
	
	public void cancel(Reservation r) {
		reservationBusiness.cancelReservation(r);
		FacesMessages.info("", "La réservation a bien été annulée.");
	}
	
	public void close(Reservation r) {
		reservationBusiness.closeReservation(r);
		FacesMessages.info("", "La clôture de la réservation a bien été enregistrée.");

	}
	
	public List<Reservation> getReservations() {
		reservations = reservationBusiness.getReservations();
		return reservations;
	}
	
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public User getConnectedUser() {
		return connectedUser;
	}

	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}

	public Set<Reservation> getConnectedUserReservations() {
		return reservationBusiness.getReservationsByUser(connectedUser);
	}

	public void setConnectedUserReservations(Set<Reservation> connectedUserReservations) {
		this.connectedUserReservations = connectedUserReservations;
	}
	
	public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
	    return dateToConvert.toInstant()
	    	      .atZone(ZoneId.systemDefault())
	    	      .toLocalDate();
	}

	public Set<Archive> getConnectedUserBasket() {
		return connectedUserBasket;
	}

	public void setConnectedUserBasket(Set<Archive> connectedUserBasket) {
		this.connectedUserBasket = connectedUserBasket;
	}

	public Date getUserAppointmentDate() {
		return userAppointmentDate;
	}

	public void setUserAppointmentDate(Date userAppointmentDate) {
		this.userAppointmentDate = userAppointmentDate;
	}

	public ReservationIBusiness getReservationBusiness() {
		return reservationBusiness;
	}

	public void setReservationBusiness(ReservationIBusiness reservationBusiness) {
		this.reservationBusiness = reservationBusiness;
	}
	
	


}
