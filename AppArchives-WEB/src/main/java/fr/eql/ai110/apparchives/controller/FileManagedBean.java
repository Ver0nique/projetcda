package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.File;
import fr.eql.ai110.apparchives.ibusiness.FileIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbFile")
@RequestScoped
public class FileManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Set<File> filesSet;
	private List<File> filesList;
	
	private Archive selectedArchive;

	@EJB
	private FileIBusiness fileBusiness;
	

	public Set<File> getFilesByArchive(Archive archive) {
		return fileBusiness.getFilesByArchive(archive);
	}
	
	
	public List<File> getFiles(){
		filesList = fileBusiness.getFiles();
		return filesList;
	}

	public FileIBusiness getFileBusiness() {
		return fileBusiness;
	}

	public void setFileBusiness(FileIBusiness fileBusiness) {
		this.fileBusiness = fileBusiness;
	}

	public Set<File> getFilesSet() {
		return filesSet;
	}

	public void setFilesSet(Set<File> filesSet) {
		this.filesSet = filesSet;
	}

	public List<File> getFilesList() {
		return filesList;
	}

	public void setFilesList(List<File> filesList) {
		this.filesList = filesList;
	}

	public Archive getSelectedArchive() {
		return selectedArchive;
	}

	public void setSelectedArchive(Archive selectedArchive) {
		this.selectedArchive = selectedArchive;
	}
	
	public void specificInfo() {
	    FacesMessages.info("growlForm:ref", "Info", "This is a specific message!");
	}
	
	public void warning() {
	    FacesMessages.warning("Warning!", "Something has gone <strong>wrong</strong>.");
	}
	
	public void error() {
	    FacesMessages.error("Error!", "Something has gone <strong>wrong</strong>.");
	}
	


}
