package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.ibusiness.ArchiveIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbSearch")
@SessionScoped
public class SearchManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ArchiveIBusiness archiveBusiness;
	
	private String callNumbers;
	private List<String> archivesCallnumbers;
	private String category;
	private List<String> categories;
	private Boolean digitalized;
	private String earliestYear;
	private String latestYear;
	private String title;
	private List<String> titles;
	private List<String> years;
	private Set <Archive> searchResults;
	
	@PostConstruct
	public void init() {
		callNumbers = "";
		title = "";
		category= null;
		earliestYear = "";
		latestYear = "";
		digitalized=null;
		search();
	}
	
	public Set<Archive> search(){
		searchResults = archiveBusiness.search(callNumbers, category, digitalized, title, earliestYear, latestYear);
		return searchResults;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getTitles() {
		titles = archiveBusiness.getArchivesTitle();
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public List<String> getCategories() {
		categories = archiveBusiness.getArchivesCategory();
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getCallNumbers() {
		return callNumbers;
	}

	public void setCallNumbers(String callNumbers) {
		this.callNumbers = callNumbers;
	}

	public Boolean getDigitalized() {
		return digitalized;
	}

	public void setDigitalized(Boolean digitalized) {
		this.digitalized = digitalized;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getArchivesCallnumbers() {
		archivesCallnumbers = archiveBusiness.getArchivesCallnumbers();
		return archivesCallnumbers;
	}

	public void setArchivesCallnumbers(List<String> archivesCallnumbers) {
		this.archivesCallnumbers = archivesCallnumbers;
	}

	public String getEarliestYear() {
		return earliestYear;
	}

	public void setEarliestYear(String earliestYear) {
		this.earliestYear = earliestYear;
	}

	public String getLatestYear() {
		return latestYear;
	}

	public void setLatestYear(String latestYear) {
		this.latestYear = latestYear;
	}

	public List<String> getYears() {
		years = archiveBusiness.getArchivesYear();
		return years;
	}

	public void setYears(List<String> years) {
		this.years = years;
	}

	public Set<Archive> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(Set<Archive> searchResults) {
		this.searchResults = searchResults;
	}
	
	
}
