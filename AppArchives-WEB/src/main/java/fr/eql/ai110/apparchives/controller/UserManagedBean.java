package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.ibusiness.AccountIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbUser")
@SessionScoped
public class UserManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private String password;
	private String lastname;
	private String firstname;
	private List<User> users;
	
	private User user;
	
	@EJB
	private AccountIBusiness accountBusiness;

	public String createUserReader() {
		String forward = null;
		User user = new User();
		user.setLogin(login);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setRole("Reader");
		if(accountBusiness.create(user, password)) {
			FacesMessages.info("", "Votre compte a été créé avec succès ! Vous pouvez à présent vous authentifier.");
			forward = "/login.xhtml?faces-redirection=true";
		}else {
			FacesMessage facesMessage = new FacesMessage(
				FacesMessage.SEVERITY_WARN,"","L'identifiant est déjà utilisé, veuillez en indiquer un autre");
			FacesContext.getCurrentInstance().addMessage("signinForm:inpLogin", facesMessage);
			forward = "/createuser.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public String connect() {
		String forward = "";
		user = accountBusiness.connect(login, password);
		if(isConnected()) {
			forward = "/search.xhtml?faces-redirection=true";
		}else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
			forward = "/login.xhtml?faces-redirection=false";
		}
		
		return forward;
	}
	
	public boolean isConnected() {
		return user != null;
	}
	
	public String checkEmployeeRole() {
		String forward = "";
		if(!user.getRole().equals("Employee")) {
			forward = "/index.xhtml";
		}
		return forward;
	}
	
	public String checkConnectedUser() {
		String forward = "";
		if(!isConnected()) {
			forward = "/index.xhtml";
		}
		return forward;
	}
	
	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		user = null;
		return "/login.xhtml?faces-redirection=true";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	
	
}
