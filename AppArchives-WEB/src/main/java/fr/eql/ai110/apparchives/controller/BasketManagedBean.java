package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.apparchives.entity.Archive;
import net.bootsfaces.utils.FacesMessages;


@ManagedBean(name="mbBasket", eager = true)
@SessionScoped
public class BasketManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Set<Archive> basket;
	
	@PostConstruct
	public void initUserBasket() {
		basket = new HashSet<>();
	}
	
	public void addToBasket(Archive archive) {
		if(!basket.contains(archive)) {
			if(basket.size()==3) {
			    FacesMessages.error("Ajout impossible !", "Vous ne pouvez pas ajouter plus de 3 archives à votre panier.");
			}else {
				basket.add(archive);
			}
		}else {
		    FacesMessages.warning("Attention !", "Cette archive est déjà dans votre panier.");
		}
	}
	
	public void removeFromBasket(Archive archive) {
		basket.remove(archive);
		FacesMessages.info("", "L'archive a bien été retirée.");
	}
	
	public String basketName(){
		return basket.size() < 2 ? "archive" : "archives";
	}

	public Set<Archive> getBasket() {
		return basket;
	}

	public void setBasket(Set<Archive> basket) {
		this.basket = basket;
	}

	
}
