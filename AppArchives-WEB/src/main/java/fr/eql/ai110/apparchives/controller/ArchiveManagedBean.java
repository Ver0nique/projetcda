package fr.eql.ai110.apparchives.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.eql.ai110.apparchives.entity.Archive;
import fr.eql.ai110.apparchives.entity.Reservation;
import fr.eql.ai110.apparchives.entity.User;
import fr.eql.ai110.apparchives.ibusiness.ArchiveIBusiness;

@ManagedBean(name="mbArchive")
@RequestScoped
public class ArchiveManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Archive> archives;
	private Archive archive;
	
	@ManagedProperty(value="#{mbUser.user}")
	private User connectedUser;
	
	@EJB
	private ArchiveIBusiness archiveBusiness;
	
	public Set<Archive> getReservedArchives(Reservation reservation){
		return archiveBusiness.getArchivesByReservation(reservation);
	}
	
	public List<Archive> getArchives(){
		archives = archiveBusiness.getArchives();
		return archives;
	}
	
	public void setArchives(List<Archive> archives) {
		this.archives = archives;
	}


	public Archive getArchive() {
		return archive;
	}


	public void setArchive(Archive archive) {
		this.archive = archive;
	}


	public User getConnectedUser() {
		return connectedUser;
	}


	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}



}
