package fr.eql.ai110.apparchives.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="reservation")
public class Reservation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="creation_date")
	private LocalDate creationDate;
	@Column(name="validation_date")
	private LocalDate validationDate;
	@Column(name="refusal_date")
	private LocalDate refusalDate;
	@Column(name="cancellation_date")
	private LocalDate cancellationDate;
	@Column(name="completion_date")
	private LocalDate completionDate;
	@Column(name="appointment_date")
	private LocalDate appointmentDate;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	@ManyToMany(cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "reservation_archive", 
    joinColumns = @JoinColumn(name = "reservation_id"), 
    inverseJoinColumns = @JoinColumn(name = "archive_id"))	
	private Set<Archive> reservedArchives;
	
	public Reservation() {
		super();
		this.reservedArchives = new HashSet<>();
	}
	
	public Reservation(Integer id, LocalDate creationDate, LocalDate validationDate, LocalDate refusalDate,
			LocalDate cancellationDate, LocalDate completionDate, LocalDate appointmentDate, User user,
			Set<Archive> reservedArchives) {
		super();
		this.id = id;
		this.creationDate = creationDate;
		this.validationDate = validationDate;
		this.refusalDate = refusalDate;
		this.cancellationDate = cancellationDate;
		this.completionDate = completionDate;
		this.appointmentDate = appointmentDate;
		this.user = user;
		this.reservedArchives = reservedArchives;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDate getValidationDate() {
		return validationDate;
	}

	public void setValidationDate(LocalDate validationDate) {
		this.validationDate = validationDate;
	}

	public LocalDate getRefusalDate() {
		return refusalDate;
	}

	public void setRefusalDate(LocalDate refusalDate) {
		this.refusalDate = refusalDate;
	}

	public LocalDate getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(LocalDate cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public LocalDate getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(LocalDate completionDate) {
		this.completionDate = completionDate;
	}

	public LocalDate getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(LocalDate appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Archive> getReservedArchives() {
		return reservedArchives;
	}

	public void setReservedArchives(Set<Archive> reservedArchives) {
		this.reservedArchives = reservedArchives;
	}

	


}
