package fr.eql.ai110.apparchives.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="archive")
public class Archive implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="title")
	private String title;
	@Column(name="earliest_date")
	private LocalDate earliestDate;
	@Column(name="latest_date")
	private LocalDate latestDate;
	@Column(name="call_numbers")
	private String callNumbers;
	@Column(name="category")
	private String category;
	@Column(name="digitalized")
	private Boolean digitalized;
	@Column(name="availability")
	private Boolean availability;
	@OneToMany(mappedBy = "archive", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<File> files;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	@ManyToMany(cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "reservation_archive", 
    joinColumns = @JoinColumn(name = "archive_id"), 
    inverseJoinColumns = @JoinColumn(name = "reservation_id"))	
	private Set<Reservation> reservations;
	
	public Archive() {
		super();
		this.reservations = new HashSet<>();
	}

	public Archive(Integer id, String title, LocalDate earliestDate, LocalDate latestDate, String callNumbers,
			String category, Boolean digitalized, Boolean availability, Set<File> files, User user,
			Set<Reservation> reservations) {
		super();
		this.id = id;
		this.title = title;
		this.earliestDate = earliestDate;
		this.latestDate = latestDate;
		this.callNumbers = callNumbers;
		this.category = category;
		this.digitalized = digitalized;
		this.availability = availability;
		this.files = files;
		this.user = user;
		this.reservations = reservations;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDate getEarliestDate() {
		return earliestDate;
	}

	public void setEarliestDate(LocalDate earliestDate) {
		this.earliestDate = earliestDate;
	}

	public LocalDate getLatestDate() {
		return latestDate;
	}

	public void setLatestDate(LocalDate latestDate) {
		this.latestDate = latestDate;
	}

	public String getCallNumbers() {
		return callNumbers;
	}

	public void setCallNumbers(String callNumbers) {
		this.callNumbers = callNumbers;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Boolean getDigitalized() {
		return digitalized;
	}

	public void setDigitalized(Boolean digitalized) {
		this.digitalized = digitalized;
	}

	public Set<File> getFiles() {
		return files;
	}

	public void setFiles(Set<File> files) {
		this.files = files;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Archive other = (Archive) obj;
		return Objects.equals(id, other.id);
	}

	
	

}
